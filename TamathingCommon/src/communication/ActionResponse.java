package communication;

import java.io.Serializable;

public class ActionResponse implements Serializable {
    private final ResponseType type;

    public ActionResponse(ResponseType type) {
        this.type = type;
    }

    public ResponseType getType() {
        return type;
    }
}
