package communication;

// Communication obejct from client to server
import java.io.Serializable;

public class ActionRequest implements Serializable {

    private final RequestType type;
    private String[] parameters;

    public ActionRequest(RequestType type) {
        this.type = type;
        this.parameters = new String[3]; // TODO: does this make sense? maybe make the size dynamic in setParameter?
    }

    public RequestType getType() {
        return type;
    }

    
    // TODO: Are string parameters enough... should we use Object type parameters instead?
    public void setParameter(int i, String text) {
        if (i >= 0 && i <= 2) {
            this.parameters[i] = text;
        }
    }

    public String getParameter(int i) {
        if (i >= 0 && i <= 2) {
            return this.parameters[i];
        }
        return "null";
    }
}
