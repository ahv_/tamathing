package communication;

public enum ResponseType {
    
     INVALID, // generic error
     
    CONNECTED, // initial connection formed to server
    LOGIN_FAIL, // invalid credentials
    LOGIN_SUCCESS,
    REGISTER_SUCCESS,
    REGISTER_FAIL, // TODO: maybe should have REGISTER_FAIL_USERTAKEN and REGISTER_FAIL_INVALIDUSER for more descripteveness
    NO_LOGIN, // not logged in, (but trying to send non-login related requests)
    
}
