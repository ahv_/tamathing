package communication;

// enum of different actions a player can take
public enum RequestType {
    
    // login
    CONNECT, LOGIN,REGISTER,
    
    // shop
    BUY_EMBRYO, BUY_PEN, BUY_TOOL, BUY_FOOD,
    SELL_EMBRYO, SELL_PEN, SELL_TOOL, SELL_FOOD,
    
    // lab management
    PLACE_PEN,
    
}
