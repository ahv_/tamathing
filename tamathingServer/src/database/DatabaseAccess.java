package database;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.BaseConnectionSource;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.h2.jdbcx.JdbcDataSource;
import org.h2.tools.DeleteDbFiles;

// Connects to the database, gets and sets values
public class DatabaseAccess {

    private static final String DB_CONNECTION = "jdbc:h2:~/test";
    private static final String DB_USER = "mock";
    private static final String DB_PASSWORD = "ingjay";

    private final Dao<Account, String> accountDao;

    public DatabaseAccess() throws SQLException {
        try {
            recreateDatabase();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseAccess.class.getName()).log(Level.SEVERE, null, ex);
        }

        ConnectionSource cs = new JdbcConnectionSource(DB_CONNECTION, DB_USER, DB_PASSWORD);
        
        this.accountDao = DaoManager.createDao(cs, Account.class);

    }

    private void recreateDatabase() throws SQLException {

        DeleteDbFiles.execute("~", "test", true); // TODO: testing phase, remove old database, recreate
        JdbcDataSource ds = new JdbcDataSource();
        ds.setURL(DB_CONNECTION);
        ds.setUser(DB_USER);
        ds.setPassword(DB_PASSWORD);
        Connection connection = ds.getConnection();
        connection.setAutoCommit(false);

        // tables
        String[] tables = {
            "CREATE TABLE Account(username varchar(255) primary key, passhash varchar(255))"
        };

        for (String s : tables) {
            PreparedStatement createStatement = connection.prepareStatement(s);
            createStatement.executeUpdate();
            createStatement.close();
        }

        String insertQuery = "INSERT INTO Account" + "(username, passhash) values" + "(?,?)"; // TODO: no hashing implemented

        PreparedStatement insertStatement = connection.prepareStatement(insertQuery);
        insertStatement.setString(1, "henry");
        insertStatement.setString(2, "pass");
        insertStatement.executeUpdate();
        insertStatement.close();

        PreparedStatement insertXirk = connection.prepareStatement(insertQuery);
        insertXirk.setString(1, "xirk");
        insertXirk.setString(2, "pass");
        insertXirk.executeUpdate();
        insertXirk.close();

        connection.commit();
    }

    public Dao<Account, String> getAccountDao() {
        return this.accountDao;
    }
}
