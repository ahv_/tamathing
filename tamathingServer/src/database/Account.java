package database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Account")
public class Account {

    @DatabaseField(id = true)
    private String username;
    @DatabaseField
    private String passhash;

    public Account() {
    }

    public Account(String username, String passhash) {
        this.username = username;
        this.passhash = passhash;
    }
    

    public String getUsername() {
        return username;
    }

    public String getPasshash() {
        return passhash;
    }

    public boolean isInFight() {
        return false; // TODO:
    }
}
