local f = {}

function f.range(env, ideal, low, high, threshold)
    local offset = env - ideal
    local offLow = offset + low
    local offHigh = offset - high
    -- öööööö....
end


-- adjust all non-resistance stats by amount, amount can be negative!
function f.adjustAllStats(s, amount)
    s.strength = s.strength + amount
    s.dexterity = s.dexterity + amount
    s.constitution = s.constitution + amount
    s.intelligence = s.intelligence + amount
    s.wisdom = s.wisdom + amount
    s.charisma = s.charisma + amount
end

return f

