package server;

import database.DatabaseAccess;
import java.io.IOException;
import java.net.ServerSocket;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServerMain {
    // TODO: implement encryption
    // http://www.jasypt.org/easy-usage.html
    // http://www.java2s.com/Code/Java/Network-Protocol/UseJavascompressionclassestoreducetheamountofdatasentoverasocket.htm

    public static void main(String[] args) {

        try {

            // Start database.
            System.out.println("Starting database..");
            DatabaseAccess dba = null;
            try {
                dba = new DatabaseAccess();
            } catch (SQLException ex) {
                Logger.getLogger(ServerMain.class.getName()).log(Level.SEVERE, null, ex);
            }

            // initialize game, load resources, modules
            Game game = new Game(); // TODO: multiple components to handle this shit...??

            // client management
            ServerSocket ss = new ServerSocket(20402);

            // Client Manager
            ClientManager clientManager = new ClientManager(ss);

            // Login Manager
            LoginManager loginManager = new LoginManager(dba);

            // Request Handler
            RequestHandler requestHandler = new RequestHandler(loginManager, clientManager.getRequests());

            // start
            Thread cmt = new Thread(clientManager);
            cmt.start();

            while (true) { // TODO: sloppy main loop
                clientManager.clientsPump();
                requestHandler.handleRequest();
            }

        } catch (IOException ex) {
            Logger.getLogger(ServerMain.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Server process ended.");
    }
}
