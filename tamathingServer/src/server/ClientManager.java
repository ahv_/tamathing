package server;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientManager implements Runnable {

    private final ServerSocket ss;
    private final HashMap<InetAddress, Client> acceptedClients;
    private final ConcurrentLinkedDeque<Socket> newSockets;
    private final ConcurrentLinkedDeque<WrappedActionRequest> requests;
    private final ArrayList<Client> ended;

    public ClientManager(ServerSocket ss) {
        this.ended = new ArrayList<>();
        this.acceptedClients = new HashMap<>();
        this.newSockets = new ConcurrentLinkedDeque<>();
        this.requests = new ConcurrentLinkedDeque<>();
        this.ss = ss;
    }

    @Override
    public void run() {
        while (!this.ss.isClosed()) {
            try {
                Socket socket = this.ss.accept();
                this.newSockets.add(socket);
                System.out.println("New connection: " + socket.getInetAddress().getHostAddress());
            } catch (IOException ex) {
                Logger.getLogger(ClientManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("ServerSocket closed, stopped listening for new connections.");
    }

    public void clientsPump() { // TODO: ehh...
        
        // Establish new connections
        while (!newSockets.isEmpty()) {
            Socket socket = this.newSockets.poll();
            InetAddress inetAddress = socket.getInetAddress();
            System.out.println("Creating a client session for " + socket.getInetAddress());
            this.acceptedClients.put(inetAddress, new Client(socket, this.requests));
        }
        
        // Listen clients
        for (Client c : this.acceptedClients.values()) {
            if (c.isAlive()) {
                c.populateComms();
            } else {
                this.ended.add(c);
            } 
        }
        
        // Remove dead clients
        for (Client c : this.ended){
            System.out.println("Ending client session for " + c);
            c.destroy();
            this.acceptedClients.remove(c.getInetAddress());
        }
        this.ended.clear();
    }

    public ConcurrentLinkedDeque<WrappedActionRequest> getRequests() {
        return requests;
    }

}
