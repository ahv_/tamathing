package server;

import communication.ActionRequest;
import communication.ActionResponse;
import communication.ResponseType;
import database.Account;
import java.util.concurrent.ConcurrentLinkedDeque;

public class RequestHandler {

    private final LoginManager loginManager;
    private final ConcurrentLinkedDeque<WrappedActionRequest> requests;

    RequestHandler(LoginManager loginManager, ConcurrentLinkedDeque<WrappedActionRequest> requests) {
        this.loginManager = loginManager;
        this.requests = requests;
    }

    public void handleRequest() {
        if (!this.requests.isEmpty()) {
            WrappedActionRequest r = this.requests.poll();
            if (!r.getClient().isLoggedIn()) {
                handleLogin(r);
            } else if (r.getClient().getAccount().isInFight()) {
                // TODO: detect if player has previously disconnected from an ongoing fight, facilitate reconnecting
                handleFight(r);
            } else {
                handleMain(r);
            }
        }
    }

    private void handleLogin(WrappedActionRequest wrappedRequest) {
        Client c = wrappedRequest.getClient();
        ActionRequest r = wrappedRequest.getRequest();
        switch (r.getType()) {

            case CONNECT:
                c.send(new ActionResponse(ResponseType.CONNECTED));
                break;

            case LOGIN:
                boolean credentialsOk = this.loginManager.checkCredentials(r.getParameter(1), r.getParameter(2));
                if (credentialsOk) {
                    System.out.println(r.getParameter(1) + " logged in.");
                    c.send(new ActionResponse(ResponseType.LOGIN_SUCCESS));
                } else {
                    System.out.println(r.getParameter(1) + " tried to log in with wrong password.");
                    c.send(new ActionResponse(ResponseType.LOGIN_FAIL));
                }
                break;
            case REGISTER:
                String username = r.getParameter(1);
                String password = r.getParameter(2);
                if (this.loginManager.accountExists(username)) {
                    c.send(new ActionResponse(ResponseType.REGISTER_FAIL));
                } else {
                    Account account = new Account(username, password);
                    c.setAccount(account);
                    this.loginManager.saveAccount(account);
                    c.send(new ActionResponse(ResponseType.REGISTER_SUCCESS));
                }
                break;
            default:
                c.send(new ActionResponse(ResponseType.NO_LOGIN));
                break;
        }
    }

    private void handleFight(WrappedActionRequest wrappedRequest) {
        Client c = wrappedRequest.getClient();
        ActionRequest r = wrappedRequest.getRequest();
        //TODO: fight
    }

    private void handleMain(WrappedActionRequest wrappedRequest) {
        Client c = wrappedRequest.getClient();
        ActionRequest r = wrappedRequest.getRequest();
        switch (r.getType()) {
            default:
                break;
        }
    }
}
