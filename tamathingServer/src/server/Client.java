package server;

import communication.ActionRequest;
import communication.ActionResponse;
import communication.StatusUpdate;
import database.Account;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Client {

    private final ConcurrentLinkedDeque<WrappedActionRequest> comms;
    private ObjectInputStream in;
    private ObjectOutputStream out;
    private Account account;
    private boolean alive;
    private final Socket socket;

    Client(Socket socket, ConcurrentLinkedDeque<WrappedActionRequest> comms) {
        this.comms = comms;
        this.socket = socket;
        try {
            this.in = new ObjectInputStream(socket.getInputStream());
            this.out = new ObjectOutputStream(socket.getOutputStream());
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.alive = true;
    }

    public void populateComms() {
        try {
            Object req = this.in.readObject();
            if (req == null) {
                return;
            }
            if (req instanceof ActionRequest) {
                this.comms.add(new WrappedActionRequest(this, (ActionRequest) req));
            }

        } catch (ClassNotFoundException | IOException ex) { // TODO: is this kosher?
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
            this.alive = false;
        }
    }

    public boolean isLoggedIn() {
        return (this.account != null);
    }

    public Account getAccount() {
        return this.account;
    }

    public void send(StatusUpdate update) {
        try {
            this.out.writeObject(update);
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
            this.alive = false;
        }
    }

    public void send(ActionResponse re) {
        try {
            this.out.writeObject(re);
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
            this.alive = false;
        }
    }

    public void setAccount(Account account) {
        this.account = account; // TODO: prevent multiple logins to same account
    }

    public boolean isAlive() {
        return this.alive;
    }

    public void destroy() {
        try {
            this.in.close();
            this.out.close();
            this.socket.close();
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public InetAddress getInetAddress() {
        return this.socket.getInetAddress();
    }
}
