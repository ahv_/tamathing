package server;

import communication.ActionRequest;
import server.Client;

// object that wraps the client reference with the request
public class WrappedActionRequest {
    private final Client client;
    private final ActionRequest request;

    public WrappedActionRequest(Client client, ActionRequest request) {
        this.client = client;
        this.request = request;
    }

    public Client getClient() {
        return this.client;
    }
    
    public ActionRequest getRequest(){
        return this.request;
    }
}
