package server;

import com.j256.ormlite.dao.Dao;
import database.Account;
import database.DatabaseAccess;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoginManager {

    private Dao<Account, String> accountDao;

    LoginManager(DatabaseAccess dba) {

        this.accountDao = dba.getAccountDao();
    }

    boolean checkCredentials(String username, String password) {
        try {
            List<Account> q = this.accountDao.queryForEq("username", username);
            if (q.isEmpty()) {
                return false;
            }
            Account a = q.get(0);
            if (a == null) {
                return false;
            }
            return (a.getPasshash().equals(password));
        } catch (SQLException ex) {
            Logger.getLogger(LoginManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    boolean accountExists(String username) {
        try {
            return !(this.accountDao.queryForEq("username", username).isEmpty());
        } catch (SQLException ex) {
            Logger.getLogger(LoginManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true; // TODO: says that account exists when query fails, not good
    }

    void saveAccount(Account account) {
        try {
            this.accountDao.update(account);
        } catch (SQLException ex) {
            Logger.getLogger(LoginManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
