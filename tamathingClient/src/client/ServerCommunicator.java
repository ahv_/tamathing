package client;

import client.frame.ClientFrame;
import client.frame.FailConnectFrame;
import client.frame.LoginFrame;
import client.frame.RegisterFrame;
import communication.ActionRequest;
import communication.ActionResponse;
import communication.ResponseType;
import communication.StatusUpdate;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServerCommunicator implements Runnable {

    private Socket socket;
    private ObjectOutputStream out;
    private ObjectInputStream in;
    private LoginFrame loginFrame;
    private ClientFrame clientFrame;
    private RegisterFrame registerFrame;

    public ServerCommunicator(String address, int port) {
        try {
            this.socket = new Socket(address, port);
            this.out = new ObjectOutputStream(socket.getOutputStream());
            this.in = new ObjectInputStream(socket.getInputStream());
        } catch (IOException ex) {
            //Logger.getLogger(ServerCommunicator.class.getName()).log(Level.SEVERE, null, ex);
            FailConnectFrame failConnectFrame = new FailConnectFrame();
            failConnectFrame.setLocationRelativeTo(null);
            failConnectFrame.setVisible(true);
        }
    }

    public void send(ActionRequest req) {
        try {
            this.out.writeObject(req);
            this.out.flush();
        } catch (IOException ex) {
            Logger.getLogger(ServerCommunicator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void run() {
        while (true) {
            try {
                Object r = this.in.readObject();
                if (r == null) {
                    return;
                }
                if (r instanceof StatusUpdate) {
                    handleStatusUpdate((StatusUpdate) r);
                } else if (r instanceof ActionResponse) {
                    handleResponse((ActionResponse) r);
                } else {
                    // TODO: Invalid/unhandled object received
                }
            } catch (IOException | ClassNotFoundException ex) {
                Logger.getLogger(ServerCommunicator.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void handleResponse(ActionResponse response) {
        ResponseType t = response.getType();
        switch (t) {
            case CONNECTED:
                this.loginFrame.setVisible(true);
                break;
                case LOGIN_FAIL:
                    // TODO: display notification about invalid credentials
                    break;
                case REGISTER_SUCCESS:
                case LOGIN_SUCCESS:
                    this.loginFrame.setVisible(false);
                    this.clientFrame.setVisible(true);
                    break;
                case REGISTER_FAIL:
                    this.registerFrame.failedRegister();
            default:
                break;
        }
    }

    private void handleStatusUpdate(StatusUpdate statusUpdate) {
        
    }

    public void setFrames(LoginFrame login, ClientFrame client, RegisterFrame register) {
        this.loginFrame = login;
        this.clientFrame = client;
        this.registerFrame = register;
    }
}
