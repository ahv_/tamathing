package client;

import client.frame.ClientFrame;
import client.frame.LoginFrame;
import client.frame.RegisterFrame;
import communication.ActionRequest;
import communication.RequestType;

public class ClientMain {

    public static void main(String[] args) {

        LoginFrame login = new LoginFrame();
        RegisterFrame register = new RegisterFrame();
        ClientFrame client = new ClientFrame();
        ServerCommunicator comm = new ServerCommunicator("localhost", 20402);
        comm.setFrames(login, client, register);
        new Thread(comm).start();
        comm.send(new ActionRequest(RequestType.CONNECT));
        register.setComms(comm);
        login.setThings(comm, register);
        client.setComms(comm);

        login.setLocationRelativeTo(null);
        client.setLocationRelativeTo(null);
        register.setLocationRelativeTo(null);
    }
}
